import 'package:app_conocer/theme/colors.dart';
import 'package:app_conocer/views/organo_de_gobierno.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:url_launcher/url_launcher.dart';

class RedesSocialesPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size.width;
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: appBar(context, svg: 'assets/img/share_2.svg'),
        body: Container(
          padding: EdgeInsets.all(15),
          width: double.infinity,
          child: Column(
            children: [
              SizedBox(height: size * .04),
              Text(
                'Redes Sociales',
                style: TextStyle(
                  color: AppColors.primary,
                  fontWeight: FontWeight.w800,
                  fontSize: size * 0.040,
                ),
              ),
              SizedBox(height: size * .1),
              SvgPicture.asset(
                'assets/img/conocer.svg',
                width: size * .90,
              ),
              SizedBox(height: size * .1),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  SizedBox(width: 20),
                  GestureDetector(
                    onTap: () => launch('https://www.facebook.com/ConocerMx'),
                    child: SvgPicture.asset(
                      'assets/img/facebook-logo-button.svg',
                      width: size * .14,
                    ),
                  ),
                  GestureDetector(
                    onTap: () => launch('https://mobile.twitter.com/conocermx'),
                    child: SvgPicture.asset(
                      'assets/img/twitter-logo-button.svg',
                      width: size * .14,
                    ),
                  ),
                  GestureDetector(
                    onTap: () =>
                        launch('https://www.youtube.com/user/CONOCERMX/'),
                    child: SvgPicture.asset(
                      'assets/img/youtube-logo.svg',
                      width: size * .14,
                    ),
                  ),
                  SizedBox(width: 20),
                ],
              )
            ],
          ),
        ));
  }
}
