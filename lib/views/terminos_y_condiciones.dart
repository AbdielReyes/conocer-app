import 'package:app_conocer/core/constants.dart';
import 'package:app_conocer/theme/colors.dart';
import 'package:app_conocer/views/organo_de_gobierno.dart';
import 'package:flutter/material.dart';

class TerminosCondicionesPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final List<_Politicas> _politicas = [
      _Politicas('Derechos de Autor',
          'Respetamos los derechos de propiedad intelectual, por lo que, convocamos a las personas que utilicen esta aplicación móvil (APP) del Consejo Nacional de Normalización y Certificación de Competencias Laborales (CONOCER), o los servicios o artículos puestos a su disposición en o a través del CONOCER, se conduzcan bajo el mismo precepto.'),
      _Politicas('Términos y Condiciones',
          'Aseguramos que la información publicada en esta app es correcta y actualizada. Nos reservamos el derecho de cambiar o hacer correcciones a cualquier información proporcionada dentro de la app en cualquier momento y sin ningún aviso previo.     CONOCER no avala, ni es responsable de la exactitud o veracidad de cualquier opinión, consejo o declaración en la app, ni de cualquier publicación ofensiva, difamatoria, obscena, indecente, ilegal o violatoria hecha en el mismo, por cualquier persona que no sea un empleado portavoz autorizado del conocer en su carácter oficial (incluyendo, sin limitación, otros usuarios de la app). Es su responsabilidad evaluar la exactitud, conclusión o utilidad de cualquier información, opinión, consejo u otro contenido disponible a través de la app.'),
      _Politicas('Aviso de Privacidad',
          'Para mayor información ponemos a su disposición, nuestra página de internet en su sección de Avisos de Privacidad en donde usted podrá consultar nuestro Aviso de Privacidad Integral, así como los mecanismos para hacer valer su derecho ARCO.')
    ];
    final size = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: appBar(context, svg: 'assets/img/handshake.svg'),
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(height: 30),
            Center(
              child: Text(
                'Términos y Condiciones de uso',
                style: TextStyle(
                    color: AppColors.primary,
                    fontWeight: FontWeight.w800,
                    fontSize: size * 0.038),
              ),
            ),
            SizedBox(height: 22),
            buildCard(
              size: size,
              name: _politicas[0].name,
              descripcion: _politicas[0].descripcion,
            ),
            buildCard(
              size: size,
              name: _politicas[1].name,
              descripcion: _politicas[1].descripcion,
            ),
            buildCard(
              size: size,
              name: _politicas[2].name,
              descripcion: _politicas[2].descripcion,
            ),
            MaterialButton(
              child: Text('Aviso de Privacidad'),
              color: AppColors.primary,
              textColor: Colors.white,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8)),
              onPressed: () => Navigator.pushNamed(context, webPageRoute,
                  arguments:
                      'https://conocer.gob.mx/contenido/avisos_de_privacidad/avisos.html'),
            )
          ],
        ),
      ),
    );
  }

  Card buildCard({String name, String descripcion, double size}) {
    return Card(
      margin: EdgeInsets.symmetric(horizontal: 15, vertical: 8),
      elevation: 4,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              name,
              style: TextStyle(
                fontSize: size * .040,
              ),
            ),
            SizedBox(height: 10),
            Text(
              descripcion,
              textAlign: TextAlign.justify,
              style: TextStyle(
                fontSize: size * .030,
                height: 1.5,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _Politicas {
  final String name;
  final String descripcion;

  _Politicas(
    this.name,
    this.descripcion,
  );
}
