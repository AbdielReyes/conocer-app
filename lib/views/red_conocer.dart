import 'package:app_conocer/core/constants.dart';
import 'package:app_conocer/theme/colors.dart';
import 'package:app_conocer/views/organo_de_gobierno.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class RedConocerPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: appBar(context, svg: 'assets/img/team.svg'),
      body: Column(
        children: [
          Expanded(
            child: buildContainer(
              text: '',
              svg: 'assets/img/quality.svg',
              text2: 'Comités de Gestión de Competencia',
              size: size,
            ),
          ),
          Divider(color: AppColors.primary),
          Expanded(
            child: buildContainer(
                text: '',
                svg: 'assets/img/diploma.svg',
                text2: 'Prestadores de Servicios',
                size: size,
                onTap: () =>
                    Navigator.pushNamed(context, prestadoresServiciosRoute)),
          ),
        ],
      ),
    );
  }

  Widget buildContainer(
      {String text, String svg, String text2, Function onTap, Size size}) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              '$text',
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 20),
            SvgPicture.asset(
              svg,
              width: size.width * .15,
            ),
            SizedBox(height: 20),
            Text(
              '$text2',
              style: TextStyle(
                fontSize: size.width * .040,
                fontWeight: FontWeight.w800,
              ),
            )
          ],
        ),
      ),
    );
  }
}
