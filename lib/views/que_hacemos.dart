import 'package:app_conocer/core/constants.dart';
import 'package:app_conocer/theme/colors.dart';
import 'package:app_conocer/views/organo_de_gobierno.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:app_conocer/core/providers/que_hacemos_data.dart';

class QueHacemosPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: appBar(context, svg: 'assets/img/information.svg'),
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(height: 15),
            Center(
              child: SvgPicture.asset(
                'assets/img/building.svg',
                color: AppColors.primary,
                width: size.width * .12,
              ),
            ),
            SizedBox(height: 20),
            Text(
              '¿Que Hacemos?',
              style: TextStyle(
                color: AppColors.primary,
                fontSize: size.width * .036,
                fontWeight: FontWeight.w800,
              ),
            ),
            ListView.builder(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemBuilder: (BuildContext context, int i) {
                final item = queHacemos[i];
                return Container(
                  padding: EdgeInsets.symmetric(vertical: 15),
                  child: ListTile(
                    title: Text(
                      '${item.titulo}',
                      textAlign: TextAlign.center,
                    ),
                    onTap: () => Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (BuildContext context) => QueHacemosItem(item),
                      ),
                    ),
                  ),
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(color: AppColors.primary),
                    ),
                  ),
                );
              },
              itemCount: queHacemos.length,
            )
          ],
        ),
      ),
    );
  }
}

class QueHacemosItem extends StatelessWidget {
  final QueHacemos item;

  const QueHacemosItem(this.item);
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final tStyle = TextStyle(
      color: AppColors.primary,
      fontSize: size.width * .050,
      fontWeight: FontWeight.w600,
    );
    final cStyle = TextStyle(
      fontSize: size.width * .032,
    );
    final lStyle = TextStyle(
      color: Colors.blue[400],
      decoration: TextDecoration.underline,
      fontSize: size.width * .032,
    );

    return Scaffold(
      appBar: appBar(context, svg: 'assets/img/information.svg'),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(25),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Center(
              child: Text(
                this.item.titulo,
                style: tStyle,
                textAlign: TextAlign.center,
              ),
            ),
            (item.contenido[0] != null)
                ? Padding(
                    padding: const EdgeInsets.only(top: 25),
                    child: Text(
                      item.contenido[0],
                      style: cStyle,
                      textAlign: TextAlign.justify,
                    ),
                  )
                : Container(),
            (item.contenido.length >= 2)
                ? GestureDetector(
                    onTap: () => Navigator.pushReplacementNamed(
                      context,
                      webPageRoute,
                      arguments: item.contenido[1],
                    ),
                    child: Text(
                      item.contenido[1],
                      style: lStyle,
                      textAlign: TextAlign.justify,
                    ),
                  )
                : Container(),
            (item.contenido.length >= 3)
                ? Padding(
                    padding: const EdgeInsets.only(top: 25),
                    child: Text(
                      item.contenido[2],
                      style: cStyle,
                      textAlign: TextAlign.justify,
                    ),
                  )
                : Container(),
            (item.contenido.length >= 4)
                ? GestureDetector(
                    onTap: () => Navigator.pushReplacementNamed(
                      context,
                      webPageRoute,
                      arguments: item.contenido[3],
                    ),
                    child: Text(
                      item.contenido[3],
                      style: lStyle,
                      textAlign: TextAlign.justify,
                    ),
                  )
                : Container(),
            (item.contenido.length >= 5)
                ? Padding(
                    padding: const EdgeInsets.only(top: 25),
                    child: Text(
                      item.contenido[4],
                      style: cStyle,
                      textAlign: TextAlign.justify,
                    ),
                  )
                : Container(),
            (item.contenido.length >= 6)
                ? GestureDetector(
                    onTap: () => Navigator.pushReplacementNamed(
                      context,
                      webPageRoute,
                      arguments: item.contenido[5],
                    ),
                    child: Text(
                      item.contenido[5],
                      style: lStyle,
                      textAlign: TextAlign.justify,
                    ),
                  )
                : Container(),
            (item.contenido.length >= 6)
                ? Padding(
                    padding: const EdgeInsets.only(top: 25),
                    child: Text(
                      item.contenido[6],
                      style: cStyle,
                      textAlign: TextAlign.justify,
                    ),
                  )
                : Container(),
            // SizedBox(height: 20),
            (item.contenido.length >= 7)
                ? GestureDetector(
                    onTap: () => Navigator.pushReplacementNamed(
                      context,
                      webPageRoute,
                      arguments: item.contenido[7],
                    ),
                    child: Text(
                      item.contenido[7],
                      style: lStyle,
                      textAlign: TextAlign.justify,
                    ),
                  )
                : Container(),
          ],
        ),
      ),
    );
  }
}
