import 'package:app_conocer/core/models/sector.dart';
import 'package:app_conocer/views/organo_de_gobierno.dart';
import 'package:flutter/material.dart';
import './sector_empresarial.dart';

class SectorGobiernoPage extends StatelessWidget {
  final List<Sector> sectores = [
    Sector(
      nombre: 'Secretaría de Educacion Pública',
      logo: 'assets/img/secretarias/sep.svg',
      url: 'https://www.google.com',
    ),
    Sector(
      nombre: 'Secretaría del Trabajo y Previsión Social',
      logo: 'assets/img/secretarias/stps.svg',
      url: 'https://www.gob.mx.stps',
    ),
    Sector(
      nombre:
          'Secretaría de Agricultura, Ganadería, Desarrollo Rural, Pesca y Alimentación',
      logo: 'assets/img/secretarias/SADER.svg',
      url: 'https://www.gob.mx.sagarpa',
    ),
    Sector(
      nombre: 'Secretaría de Turismo',
      logo: 'assets/img/secretarias/sectur.svg',
      url: 'https://www.gob.mx.sectur',
    ),
    Sector(
      nombre: 'Secretaría de Energía',
      logo: 'assets/img/secretarias/sener.svg',
      url: 'https://www.gob.mx.sener',
    ),
    Sector(
      nombre: 'Secretaría de Hacienda y Crédito Público',
      logo: 'assets/img/secretarias/shpc.svg',
      url: 'https://www.gob.mx.shcp',
    ),
    Sector(
      nombre: 'Intituto Nacional para la Educación de los Adultos',
      logo: 'assets/img/secretarias/INEA.svg',
      url: 'https://www.gob.mx.shcp',
    ),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: appBar(
        context,
        svg: 'assets/img/handshake-white.svg',
      ),
      body: orgGobBody(sectores),
    );
  }
}
