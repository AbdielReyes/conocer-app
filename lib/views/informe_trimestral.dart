import 'package:app_conocer/core/constants.dart';
import 'package:app_conocer/theme/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:url_launcher/url_launcher.dart';

import 'organo_de_gobierno.dart';

class InformeTrimestralPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final List<Informe> _list = [
      Informe(
          'Informe Trimestral sobre el Desarrollo de Capital Humano en México, Julio-Septiembre de 2017',
          'https://conocer.gob.mx/wp-content/uploads/2017/05/Informe_trimestral_jul_sep.pdf'),
      Informe(
          'Informe Trimestral sobre el Desarrollo de Capital Humano en México, Abril-Junio de 2017',
          'https://conocer.gob.mx/wp-content/uploads/2017/05/Informe_Trimestral_Abril_junio.pdf'),
      Informe(
          'Informe Trimestral sobre el Desarrollo de Capital Humano en México, Enero-Marzo de 2017',
          'https://conocer.gob.mx/wp-content/uploads/2017/05/Info_trimestral_ene_mar.pdf'),
      Informe(
          'Informe Trimestral sobre el Desarrollo de Capital Humano en México, Octubre-Diciembre de 2016',
          'https://conocer.gob.mx/wp-content/uploads/2017/06/3erInformeTrimestralfinal.pdf'),
      Informe(
          'Informe Trimestral sobre el Desarrollo de Capital Humano en México, Julio-Septiembre de 2016',
          'https://conocer.gob.mx/wp-content/uploads/2017/05/Informe_Trimestral_digital2.pdf'),
      Informe(
          'Informe Trimestral sobre el Desarrollo de Capital Humano en México, Abril-Junio de 2016',
          'https://conocer.gob.mx/wp-content/uploads/2017/05/Informe_Trimestral_digital.pdf'),
    ];
    final size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: appBar(context, svg: 'assets/img/bullets.svg'),
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(height: 15),
            Text(
              'Informe Trimestral',
              style: TextStyle(
                color: AppColors.primary,
                fontSize: size.width * .036,
                fontWeight: FontWeight.w800,
              ),
            ),
            ListView.builder(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemBuilder: (BuildContext context, int i) {
                final item = _list[i];
                return Container(
                  padding: EdgeInsets.symmetric(vertical: 15),
                  child: ListTile(
                      title: SvgPicture.asset(
                        'assets/img/link.svg',
                        width: size.width * .10,
                      ),
                      subtitle: Padding(
                        padding: const EdgeInsets.only(top: 20),
                        child: Text(
                          item.titulo,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: size.width * .035,
                            color: Colors.blue[400],
                            decoration: TextDecoration.underline,
                          ),
                        ),
                      ),
                      onTap: () => launch(item.link)
                      //  Navigator.pushReplacementNamed(
                      //   context,
                      //   webPageRoute,
                      //   arguments: item.link,
                      // ),
                      ),
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(color: AppColors.primary),
                    ),
                  ),
                );
              },
              itemCount: _list.length,
            )
          ],
        ),
      ),
    );
  }
}

class Informe {
  final String titulo;
  final String link;

  Informe(this.titulo, this.link);
}
