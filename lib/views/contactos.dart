import 'package:app_conocer/theme/colors.dart';
import 'package:app_conocer/views/organo_de_gobierno.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:url_launcher/url_launcher.dart';

class ContactoPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    // final Uri _emailLaunchUri = Uri(
    //     scheme: 'mailto',
    //     path: 'contacto@conocer.gob.mx',
    //     queryParameters: {'subject': 'Enviado desde la APP CONOCER'});
    return Scaffold(
      appBar: appBar(context, svg: 'assets/img/email_1.svg'),
      body: Column(
        children: [
          SizedBox(height: size.height * 0.04),
          Center(
            child: Text(
              'Contacto',
              style: TextStyle(
                color: AppColors.primary,
                fontSize: size.width * .038,
                fontWeight: FontWeight.w800,
              ),
            ),
          ),
          SizedBox(height: size.height * 0.04),
          GestureDetector(
            onTap: () {
              launch('mailto:contacto@conocer.gob.mx');
            },
            child: SvgPicture.asset(
              'assets/img/email_2.svg',
              width: size.width * 0.15,
            ),
          ),
          SizedBox(height: size.height * 0.02),
          Center(
            child: Text(
              'contacto@conocer.gob.mx',
              style: TextStyle(
                fontSize: size.width * .035,
                fontWeight: FontWeight.w800,
              ),
            ),
          ),
          SizedBox(height: size.height * 0.04),
          GestureDetector(
            onTap: () => launch('tel://01800282666'),
            child: SvgPicture.asset(
              'assets/img/phone-book.svg',
              width: size.width * 0.15,
            ),
          ),
          SizedBox(height: size.height * 0.02),
          Center(
            child: Text(
              '01 800 288 26 66',
              style: TextStyle(
                fontSize: size.width * .035,
                fontWeight: FontWeight.w800,
              ),
            ),
          ),
          SizedBox(height: size.height * 0.04),
          GestureDetector(
            onTap: () => launch(
              'tel://22820200',
            ),
            child: SvgPicture.asset(
              'assets/img/phone-book.svg',
              width: size.width * 0.15,
            ),
          ),
          SizedBox(height: size.height * 0.02),
          Center(
            child: Text(
              '2282 0200',
              style: TextStyle(
                fontSize: size.width * .035,
                fontWeight: FontWeight.w800,
              ),
            ),
          )
        ],
      ),
    );
  }
}
