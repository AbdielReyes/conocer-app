import 'package:app_conocer/core/models/entidad_federativa.dart';
import 'package:app_conocer/theme/colors.dart';
import 'package:app_conocer/views/organo_de_gobierno.dart';
import 'package:flutter/material.dart';

class PrestadoresServiciosPage extends StatefulWidget {
  @override
  _PrestadoresServiciosPageState createState() =>
      _PrestadoresServiciosPageState();
}

class _PrestadoresServiciosPageState extends State<PrestadoresServiciosPage> {
  List<EntidadFederativa> _opciones = [];
  String _opcionSeleccionada;

  @override
  void initState() {
    super.initState();
    setState(() {
      this._opciones = _getEntidadesFederativas();
    });
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: appBar(context, svg: 'assets/img/team.svg'),
      body: Container(
        width: double.infinity,
        child: SingleChildScrollView(
          padding: EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: size.width * .05),
              Center(
                child: Text(
                  'Prestadores de Servicio',
                  style: TextStyle(
                    color: AppColors.primary,
                    fontSize: size.width * .040,
                    fontWeight: FontWeight.w800,
                  ),
                ),
              ),
              SizedBox(height: size.width * .05),
              Text('Entidad Federativa'),
              DropdownButton(
                isExpanded: true,
                items: _opcionesEntidades(opciones: _opciones),
                value: _opcionSeleccionada,
                hint: Text('Seleccione una Entidad Federativa'),
                onChanged: (value) {
                  setState(() {
                    _opcionSeleccionada = value;
                  });
                },
                style: TextStyle(
                  fontSize: size.width * .035,
                  color: Colors.black,
                  fontWeight: FontWeight.w400,
                ),
              ),
              TextFormField(
                cursorColor: AppColors.primary,
                decoration: InputDecoration(
                  labelText: 'Código',
                  hintText: 'Ingrese Código',
                ),
              ),
              SizedBox(height: size.width * .03),
              Row(
                children: [
                  Spacer(),
                  Icon(
                    Icons.search_outlined,
                    color: AppColors.primary,
                    size: size.width * .030,
                  ),
                  GestureDetector(
                    onTap: () {},
                    child: Text(
                      'BUSCAR',
                      style: TextStyle(
                        color: AppColors.primary,
                        fontWeight: FontWeight.w800,
                        fontSize: size.width * .030,
                      ),
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  List<DropdownMenuItem<String>> _opcionesEntidades(
      {List<EntidadFederativa> opciones}) {
    List<DropdownMenuItem<String>> lista = [];
    opciones.forEach((opcion) {
      lista.add(DropdownMenuItem(
        child: Text(opcion.name),
        value: opcion.name,
      ));
    });
    return lista;
  }
}

List<EntidadFederativa> _getEntidadesFederativas() {
  final list = EntidadesFederativasList.fromJson(data);
  return list.entidades;
}

class EntidadesFederativasList {
  final List<EntidadFederativa> entidades;
  EntidadesFederativasList(this.entidades);
  factory EntidadesFederativasList.fromJson(List<dynamic> parsedJson) {
    List<EntidadFederativa> entidades = [];
    entidades = parsedJson.map((e) => EntidadFederativa.fromJson(e)).toList();
    return new EntidadesFederativasList(entidades);
  }
}

final List<dynamic> data = [
  {"id": 1, "name": "Aguascalientes"},
  {"id": 2, "name": "Baja California"},
  {"id": 3, "name": "Baja California Sur"},
  {"id": 4, "name": "Campeche"},
  {"id": 5, "name": "Coahuila"},
  {"id": 6, "name": "Colima"},
  {"id": 7, "name": "Chiapas"},
  {"id": 8, "name": "Chihuahua"},
  {"id": 9, "name": "Ciudad de México"},
  {"id": 10, "name": "Durango"},
  {"id": 11, "name": "Guanajuato"},
  {"id": 12, "name": "Guerrero"},
  {"id": 13, "name": "Hidalgo"},
  {"id": 14, "name": "Jalisco"},
  {"id": 15, "name": "México"},
  {"id": 16, "name": "Michoacán"},
  {"id": 17, "name": "Morelos"},
  {"id": 18, "name": "Nayarit"},
  {"id": 19, "name": "Nuevo León"},
  {"id": 20, "name": "Oaxaca"},
  {"id": 21, "name": "Puebla"},
  {"id": 22, "name": "Querétaro"},
  {"id": 23, "name": "Quintana Roo"},
  {"id": 24, "name": "San Luis Potosí"},
  {"id": 25, "name": "Sinaloa"},
  {"id": 26, "name": "Sonora"},
  {"id": 27, "name": "Tabasco"},
  {"id": 28, "name": "Tamaulipas"},
  {"id": 29, "name": "Tlaxcala"},
  {"id": 30, "name": "Veracruz"},
  {"id": 31, "name": "Yucatán"},
  {"id": 32, "name": "Zacatecas"}
];
