import 'package:app_conocer/core/constants.dart';
import 'package:app_conocer/theme/colors.dart';
import 'package:app_conocer/views/organo_de_gobierno.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class EstadisticasPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: appBar(context, svg: 'assets/img/bar-chart-red.svg'),
      body: Column(
        children: [
          Expanded(
            child: buildContainer(
              text: 'Certificaciones totales emitidas al mes\n por el CONOCER',
              svg: 'assets/img/statistics.svg',
              text2: 'Mes',
              onTap: () => Navigator.pushNamed(context, estadisticasMesRoute),
              size: size,
            ),
          ),
          Divider(color: AppColors.primary),
          Expanded(
            child: buildContainer(
              text: 'Certificaciones totales emitidas al año\n por el CONOCER',
              svg: 'assets/img/line-chart.svg',
              text2: 'Año',
              size: size,
            ),
          ),
        ],
      ),
    );
  }

  Widget buildContainer(
      {String text, String svg, String text2, Function onTap, Size size}) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              '$text',
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 20),
            SvgPicture.asset(
              svg,
              width: size.width * .15,
            ),
            SizedBox(height: 20),
            Text(
              '$text2',
              style: TextStyle(
                fontWeight: FontWeight.w800,
                fontSize: size.width * .040,
              ),
            )
          ],
        ),
      ),
    );
  }
}
