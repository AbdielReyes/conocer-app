import 'package:app_conocer/core/constants.dart';
import 'package:app_conocer/core/providers/usuario_provider.dart';
import 'package:app_conocer/theme/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size.width;
    final user = Provider.of<UsuarioProvider>(context).usuario;
    return Scaffold(
      drawer: drawer(context),
      appBar: AppBar(
        leadingWidth: size * .27,
        automaticallyImplyLeading: true,
        // title: title(context),
        leading: (user != null)
            ? Builder(
                builder: (BuildContext context) {
                  return Align(
                    alignment: Alignment.centerLeft,
                    child: IconButton(
                      padding: EdgeInsets.zero,
                      icon: SvgPicture.asset(
                        'assets/img/menu.svg',
                        color: Colors.white,
                        width: size * .07,
                      ),
                      onPressed: () => Scaffold.of(context).openDrawer(),
                    ),
                  );
                },
              )
            : title(context),
        backgroundColor: AppColors.primary,
      ),
      body: bodyHome(context),
      bottomNavigationBar: terminosHome(context),
    );
  }

  Drawer drawer(BuildContext context) {
    final user = Provider.of<UsuarioProvider>(context);
    final size = MediaQuery.of(context).size.width;
    return Drawer(
      child: Column(
        children: [
          Container(
            color: AppColors.primary,
            child: Padding(
              padding: EdgeInsets.fromLTRB(12, 35, 12, 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Menu',
                    style: TextStyle(
                      fontSize: size * .045,
                      color: Colors.white,
                      fontWeight: FontWeight.w800,
                    ),
                  ),
                  GestureDetector(
                    onTap: () => Navigator.pop(context),
                    child: SvgPicture.asset(
                      'assets/img/close-button.svg',
                      width: size * .08,
                    ),
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 25.0, vertical: 25.0),
              width: double.infinity,
              color: Colors.black87,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SvgPicture.asset(
                    'assets/img/logoblanco.svg',
                    width: size * .26,
                    color: Colors.white,
                  ),
                  ListTile(
                    onTap: () {
                      Navigator.pop(context);
                      Navigator.pushNamed(context, perfilRoute);
                    },
                    contentPadding:
                        EdgeInsets.symmetric(horizontal: 0, vertical: 25),
                    leading: SvgPicture.asset(
                      'assets/img/user_black.svg',
                      color: Colors.white,
                      width: size * .08,
                    ),
                    title: Text(
                      'Mi Perfil',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: size * .040,
                      ),
                    ),
                  ),
                  ListTile(
                    onTap: () {
                      Navigator.pop(context);
                      Navigator.pushNamed(context, eventosRoute);
                    },
                    contentPadding: EdgeInsets.zero,
                    leading: SvgPicture.asset(
                      'assets/img/calendar-white.svg',
                      color: Colors.white,
                      width: size * .08,
                    ),
                    title: Text(
                      'Eventos',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: size * .040,
                      ),
                    ),
                  ),
                  Spacer(),
                  ListTile(
                    onTap: () {
                      user.usuario = null;
                      Navigator.pushReplacementNamed(context, loginRoute);
                    },
                    contentPadding: EdgeInsets.zero,
                    leading: SvgPicture.asset(
                      'assets/img/old_logoutw.svg',
                      color: Colors.white,
                      width: size * .08,
                    ),
                    title: Text(
                      'Cerrar Sesión',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: size * .040,
                      ),
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget title(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.pushNamed(context, loginRoute),
      child: Row(
        children: [
          SizedBox(width: 10),
          Icon(
            Icons.account_circle_outlined,
            color: Colors.white,
            size: MediaQuery.of(context).size.width * .040,
          ),
          SizedBox(width: 5.0),
          Text(
            'IDENTIFICATE',
            style:
                TextStyle(fontSize: MediaQuery.of(context).size.width * .030),
          )
        ],
      ),
    );
  }

  GridView bodyHome(BuildContext context) {
    return GridView(
      physics: BouncingScrollPhysics(),
      padding: EdgeInsets.symmetric(vertical: 30, horizontal: 10),
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 4,
        crossAxisSpacing: 3,
        mainAxisSpacing: 3,
        childAspectRatio: 0.8,
      ),
      children: [
        itemHome(
          context,
          img: 'assets/img/handshake.svg',
          name: 'Órgano de gobierno',
          ontap: () => Navigator.pushNamed(context, organoDeGobiernoRoute),
        ),
        itemHome(
          context,
          img: 'assets/img/information.svg',
          name: '¿Qué hacemos?',
          ontap: () => Navigator.pushNamed(context, queHacemosRoute),
        ),
        itemHome(
          context,
          img: 'assets/img/bar-chart-red.svg',
          name: 'Estadisticas',
          ontap: () => Navigator.pushNamed(context, estadisticasRoute),
        ),
        itemHome(
          context,
          img: 'assets/img/team.svg',
          name: 'Red CONOCER',
          ontap: () => Navigator.pushNamed(context, redConocerRoute),
        ),
        itemHome(
          context,
          img: 'assets/img/document.svg',
          name: 'Estandares',
          ontap: () => Navigator.pushNamed(context, estandaresRoute),
        ),
        itemHome(
          context,
          img: 'assets/img/newspaper.svg',
          name: 'Notícias',
          ontap: () => Navigator.pushNamed(
            context,
            webPageRoute,
            arguments: 'https://conocer.gob.mx/prensa/',
          ),
        ),
        itemHome(
          context,
          img: 'assets/img/envelope.svg',
          name: 'Centro Virtual de Conocimiento',
          ontap: () => Navigator.pushNamed(
            context,
            webPageRoute,
            arguments: 'https://cvc.conocer.gob.mx',
          ),
        ),
        itemHome(
          context,
          img: 'assets/img/email_1.svg',
          name: 'Contacto',
          ontap: () => Navigator.pushNamed(context, contactoRoute),
        ),
        itemHome(
          context,
          img: 'assets/img/picture.svg',
          name: 'Galería',
          ontap: () => Navigator.pushNamed(
            context,
            webPageRoute,
            arguments: 'Https://conocer.gob.mx/tipo/imagenes/',
          ),
        ),
        itemHome(
          context,
          img: 'assets/img/play-button.svg',
          name: 'Video',
          ontap: () => Navigator.pushNamed(
            context,
            webPageRoute,
            arguments: 'Https://conocer.gob.mx/tipo/video/',
          ),
        ),
        itemHome(
          context,
          img: 'assets/img/share_2.svg',
          name: 'Redes Sociales',
          ontap: () => Navigator.pushNamed(context, redesSocialesRoute),
        ),
        itemHome(
          context,
          img: 'assets/img/list.svg',
          name: 'Informe Trimestral',
          ontap: () => Navigator.pushNamed(context, informeTrimestralRoute),
        ),
        itemHome(
          context,
          img: 'assets/img/notepad_3.svg',
          name: 'Encuestas',
          ontap: () => Navigator.pushNamed(context, encuestasRoute),
        ),
        itemHome(
          context,
          img: 'assets/img/question-mark.svg',
          name: 'Preguntas Frecuentes',
          ontap: () => Navigator.pushNamed(
            context,
            webPageRoute,
            arguments: 'Https://conocer.gob.mx/preguntas-frecuentes/',
          ),
        ),
      ],
    );
  }

  Widget itemHome(
    BuildContext context, {
    String img,
    String name,
    Function ontap,
  }) {
    final size = MediaQuery.of(context).size.width;
    return GestureDetector(
      onTap: ontap,
      child: Column(
        // mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SvgPicture.asset(
            img,
            color: AppColors.primary,
            width: size * .10,
          ),
          SizedBox(height: 8),
          Text(
            '$name',
            style: TextStyle(fontSize: size * .032),
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }

  Widget terminosHome(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      height: size.height * .07,
      child: Stack(
        children: [
          Center(
            child: GestureDetector(
              onTap: () =>
                  Navigator.pushNamed(context, terminosCondicionesRoute),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Términos y condiciones de uso',
                    style: TextStyle(
                        decoration: TextDecoration.underline,
                        fontSize: size.width * .035),
                  ),
                  Text('version: $version')
                ],
              ),
            ),
          ),
          Positioned(
            right: 20,
            bottom: 20,
            child: GestureDetector(
              onTap: () => Navigator.pushNamed(context, webPageRoute,
                  arguments:
                      'https://conocer.gob.mx/aplicacion-movil-conocer/'),
              child: SvgPicture.asset(
                'assets/img/question-mark.svg',
                width: size.width * .040,
                color: AppColors.primary,
              ),
            ),
          )
        ],
      ),
    );
  }
}
