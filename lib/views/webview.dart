import 'package:app_conocer/views/organo_de_gobierno.dart';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final url = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      appBar: appBar(context),
      body: WebView(initialUrl: '$url'),
    );
  }
}
