import 'package:app_conocer/core/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class OrganoDeGobiernoPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: appBar(
        context,
        svg: 'assets/img/handshake-white.svg',
      ),
      body: ListView(
        children: [
          ListTile(
            onTap: () => Navigator.pushNamed(context, directorGeneralRoute),
            contentPadding: EdgeInsets.only(top: 10, left: 20),
            leading: CircleAvatar(
              backgroundImage: AssetImage(
                  'assets/img/Rodrigo_Alejandro_Rojas_Navarrete.jpg'),
              radius: size.width * .070,
            ),
            title: Text('Rodrigo A. Rojas Navarrete'),
            subtitle: Text('Director General del CONOCER'),
          ),
          Divider(color: Colors.red),
          ListTile(
            contentPadding: EdgeInsets.only(left: 20),
            title: Text('Sector de Gobierno'),
            onTap: () => Navigator.pushNamed(context, sectorGobiernoRoute),
          ),
          Divider(color: Colors.red),
          ListTile(
            contentPadding: EdgeInsets.only(left: 20),
            title: Text('Sector de Empresarial'),
            onTap: () => Navigator.pushNamed(context, sectorEmpresarialRoute),
          ),
          Divider(color: Colors.red),
          ListTile(
            contentPadding: EdgeInsets.only(left: 20),
            title: Text('Sector de Laboral'),
            onTap: () => Navigator.pushNamed(context, sectorLaboralRoute),
          ),
          Divider(color: Colors.red),
        ],
      ),
    );
  }
}

Widget appBar(BuildContext context, {String svg}) {
  final size = MediaQuery.of(context).size;
  return AppBar(
    leading: IconButton(
      icon: SvgPicture.asset(
        'assets/img/back.svg',
        width: size.width * .08,
        color: Colors.white,
      ),
      onPressed: () => Navigator.of(context).pop(),
    ),
    centerTitle: true,
    title: (svg != null)
        ? SvgPicture.asset(
            '$svg',
            color: Colors.white,
            width: size.width * .09,
          )
        : Container(),
    actions: [
      SvgPicture.asset(
        'assets/img/logoblanco.svg',
        color: Colors.white,
        colorBlendMode: BlendMode.srcIn,
        width: size.width * .16,
      ),
      SizedBox(
        width: 20,
      )
    ],
  );
}
