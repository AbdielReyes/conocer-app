import 'package:app_conocer/core/constants.dart';
import 'package:app_conocer/core/data/database.dart';
import 'package:app_conocer/core/models/usuario.dart';
import 'package:app_conocer/core/providers/usuario_provider.dart';
import 'package:app_conocer/core/utils.dart';
import 'package:app_conocer/theme/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import '../core/providers/login_provider.dart';

final _formKey = GlobalKey<FormState>();

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final lp = Provider.of<LoginProvider>(context);
    return Form(
      // autovalidateMode: AutovalidateMode.onUserInteraction,
      key: _formKey,
      child: Scaffold(
        body: Container(
          width: double.infinity,
          child: SingleChildScrollView(
            padding: EdgeInsets.all(15),
            child: Column(
              children: [
                SvgPicture.asset(
                  'assets/img/conocer.svg',
                  height: size.width * .45,
                  width: size.width * .45,
                ),
                SizedBox(height: size.height * .04),
                TextFormField(
                  cursorColor: AppColors.primary,
                  decoration: InputDecoration(
                    labelText: 'CORREO ELECTRÓNICO',
                    hintText: 'Escribir correo electronico',
                    icon: SvgPicture.asset(
                      'assets/img/user_black.svg',
                      width: size.width * .08,
                    ),
                  ),
                  onChanged: lp.setEmail,
                  validator: emailValidator,
                ),
                SizedBox(height: size.height * .02),
                TextFormField(
                  enableInteractiveSelection: false,
                  cursorColor: AppColors.primary,
                  decoration: InputDecoration(
                    labelText: 'CONTRASEÑA',
                    hintText: 'Escribir correo contraseña',
                    icon: SvgPicture.asset(
                      'assets/img/locked.svg',
                      width: size.width * .08,
                    ),
                  ),
                  onChanged: lp.setPassword,
                  validator: passValidator,
                ),
                SizedBox(height: size.height * .06),
                MaterialButton(
                  onPressed: () {
                    if (_formKey.currentState.validate()) {
                      DBHelper dbHelper = DBHelper();
                      dbHelper
                          .getUser(lp.email, lp.password)
                          .then((List<User> users) {
                        if (users != null && users.length > 0) {
                          final userProv = Provider.of<UsuarioProvider>(context,
                              listen: false);
                          userProv.usuario = users[0];
                          Navigator.pushReplacementNamed(context, homeRoute);
                        } else {
                          ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(
                              duration: Duration(milliseconds: 3000),
                              content: Text(
                                'Email o contraseña incorrecto',
                              ),
                            ),
                          );
                        }
                      });
                    }
                  },
                  textColor: Colors.white,
                  child: Padding(
                    padding: EdgeInsets.symmetric(
                      vertical: 19,
                    ),
                    child: Text(
                      'ENTRAR',
                      style: TextStyle(
                        fontSize: size.width * .040,
                        fontWeight: FontWeight.w800,
                      ),
                    ),
                  ),
                  color: AppColors.primary,
                  minWidth: size.width * .85,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(60),
                  ),
                ),
                SizedBox(height: size.height * .03),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text('¿NO TIENES CUENTA AÚN?'),
                    SizedBox(width: 15),
                    GestureDetector(
                      onTap: () => Navigator.pushReplacementNamed(
                          context, registroRoute),
                      child: Text(
                        'INSCRÍBETE',
                        style: TextStyle(
                          color: AppColors.primary,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: size.height * .04),
                GestureDetector(
                  onTap: () =>
                      Navigator.pushReplacementNamed(context, homeRoute),
                  child: Text(
                    'ACCEDE SIN USUARIO',
                    style: TextStyle(
                      color: AppColors.primary,
                    ),
                  ),
                ),
                SizedBox(height: size.height * .04),
                Text('OLVIDÉ MI CONTRASEÑA'),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
