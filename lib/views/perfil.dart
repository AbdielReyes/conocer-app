import 'package:app_conocer/core/providers/usuario_provider.dart';
import 'package:app_conocer/theme/colors.dart';
import 'package:app_conocer/views/organo_de_gobierno.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

class PerfilPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final user = Provider.of<UsuarioProvider>(context).usuario;

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: appBar(context, svg: 'assets/img/user_black.svg'),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: size.height * .010),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Cambiar Contraseña',
                  style: TextStyle(
                      color: AppColors.primary,
                      fontSize: size.width * .035,
                      fontWeight: FontWeight.w600),
                ),
                Text(
                  'Editar Información',
                  style: TextStyle(
                      color: AppColors.primary,
                      fontSize: size.width * .035,
                      fontWeight: FontWeight.w600),
                ),
              ],
            ),
            SizedBox(height: size.height * .030),
            Center(
              child: SvgPicture.asset(
                'assets/img/user_grey.svg',
                width: size.width * .40,
              ),
            ),
            SizedBox(height: size.height * .025),
            Center(
              child: Text(
                '${user.name}',
                style: TextStyle(
                    fontSize: size.width * .050, fontWeight: FontWeight.w600),
              ),
            ),
            _data(size, 'Nombre', '${user.name}'),
            _data(size, 'Email', '${user.email}'),
            _data(size, 'Teléfono', '${user.phone}'),
            _data(size, 'Institución', '${user.institucion}'),
            SizedBox(height: size.height * .025),
            Text(
              'Redes Sociales',
              style: TextStyle(
                color: AppColors.primary,
                fontSize: size.width * .055,
                fontWeight: FontWeight.w600,
              ),
            ),
            SizedBox(height: size.height * .025),
            _redSocial(
              size, 'facebook', 'assets/img/facebook-logo-button.svg',
              '${user.facebook}',
              // 'http://facebook.com/miperfil',
            ),
            _redSocial(
              size, 'twitter', 'assets/img/twitter-logo-button.svg',
              '${user.twitter}',
              // 'http://twitter.com/miperfil',
            ),
            _redSocial(
              size, 'linkedin', 'assets/img/linkedin-logo-button.svg',
              '${user.linkedin}',
              // 'http://linkedin.com/miperfil',
            ),
          ],
        ),
      ),
    );
  }

  Widget _data(Size size, String titulo, String data) {
    final tituloStyle = TextStyle(fontSize: size.width * .037);
    final dataStyle =
        TextStyle(color: Colors.grey, fontSize: size.width * .035);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('$titulo', style: tituloStyle),
        SizedBox(height: 8),
        Text('$data', style: dataStyle),
        Divider(color: Colors.grey),
        SizedBox(height: 10)
      ],
    );
  }

  Widget _redSocial(Size size, String red, String logo, String link) {
    final tituloStyle = TextStyle(fontSize: size.width * .037);
    final dataStyle =
        TextStyle(color: Colors.grey, fontSize: size.width * .035);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            SvgPicture.asset(
              '$logo',
              width: size.width * .037,
            ),
            SizedBox(width: 5),
            Text(
              '$red',
              style: tituloStyle,
            )
          ],
        ),
        SizedBox(height: 8),
        Text('$link', style: dataStyle),
        Divider(color: Colors.grey),
        SizedBox(height: 10),
      ],
    );
  }
}
