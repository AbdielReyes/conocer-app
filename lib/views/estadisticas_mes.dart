import 'package:app_conocer/theme/colors.dart';
import 'package:app_conocer/views/organo_de_gobierno.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';

class EstadisticasMesPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(context, svg: 'assets/img/bar-chart-red.svg'),
      body: Center(
        child: Container(
          child: BarChart(
            BarChartData(
              alignment: BarChartAlignment.center,
              borderData: FlBorderData(
                border: Border(
                  bottom: BorderSide(
                    color: AppColors.primary,
                    width: 2,
                  ),
                  left: BorderSide(
                    color: AppColors.primary,
                    width: 2,
                  ),
                ),
              ),
              gridData: FlGridData(drawHorizontalLine: true),
              axisTitleData: FlAxisTitleData(
                bottomTitle: AxisTitle(
                  margin: 0,
                  showTitle: true,
                  titleText: 'Marzo',
                ),
              ),
              maxY: 30,
              minY: 10,
              barGroups: [
                BarChartGroupData(
                  barsSpace: 10,
                  x: 1,
                  barRods: [
                    BarChartRodData(
                      borderRadius: BorderRadius.zero,
                      y: 12,
                      colors: [Colors.purple],
                      width: 40,
                    ),
                    BarChartRodData(
                      borderRadius: BorderRadius.zero,
                      y: 30,
                      colors: [Colors.pink],
                      width: 40,
                    ),
                    BarChartRodData(
                      borderRadius: BorderRadius.zero,
                      y: 13,
                      colors: [Colors.green],
                      width: 40,
                    ),
                    BarChartRodData(
                      borderRadius: BorderRadius.zero,
                      y: 16,
                      colors: [Colors.blue],
                      width: 40,
                    ),
                    BarChartRodData(
                      borderRadius: BorderRadius.zero,
                      y: 24,
                      colors: [
                        Colors.orange,
                      ],
                      width: 40,
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
