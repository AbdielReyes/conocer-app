import 'package:app_conocer/core/models/sector.dart';
import 'package:app_conocer/views/organo_de_gobierno.dart';
import 'package:flutter/material.dart';
import './sector_empresarial.dart';

class SectorLaboralPage extends StatelessWidget {
  final List<Sector> _sectores = [
    Sector(
      nombre: 'Congreso del Trabajo',
      logo: 'assets/img/secretarias/Trabajo.svg',
      url: 'https://www.gob.mx.sep',
    ),
    Sector(
      nombre: 'Confederación Revolucionaria de Obreros y Campesinos',
      logo: 'assets/img/secretarias/CROC.svg',
      url: 'https://www.gob.mx.stps',
    ),
    Sector(
      nombre: 'Confederación de Trabajadores de México',
      logo: 'assets/img/secretarias/CTM.svg',
      url: 'https://www.gob.mx.sagarpa',
    ),
    Sector(
      nombre: 'Federación Nacional de Sindicatos Independientes',
      logo: 'assets/img/secretarias/FNSI.svg',
      url: 'https://www.gob.mx.sectur',
    ),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: appBar(context, svg: 'assets/img/handshake-white.svg'),
        body: orgGobBody(_sectores));
  }
}
