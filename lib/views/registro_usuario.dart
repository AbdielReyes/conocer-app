import 'package:app_conocer/core/constants.dart';
import 'package:app_conocer/core/data/database.dart';
import 'package:app_conocer/core/providers/registro_usuario_provider.dart';
import 'package:app_conocer/core/utils.dart';
import 'package:app_conocer/theme/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';

final _formKey = GlobalKey<FormState>();

class RegistroUsuarioPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final p = Provider.of<RegistroUsuarioProvider>(context);
    return Form(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      key: _formKey,
      child: Scaffold(
        body: Container(
          width: double.infinity,
          child: SingleChildScrollView(
            padding: EdgeInsets.all(15),
            child: Column(
              children: [
                SvgPicture.asset(
                  'assets/img/conocer.svg',
                  height: size.width * .45,
                  width: size.width * .45,
                ),
                SizedBox(height: size.height * .04),
                TextFormField(
                  cursorColor: AppColors.primary,
                  decoration: InputDecoration(
                    labelText: 'NOMBRE',
                    hintText: 'Nombre de usuario',
                  ),
                  onChanged: p.setNombre,
                  validator: nameValidator,
                ),
                SizedBox(height: size.height * .02),
                TextFormField(
                  keyboardType: TextInputType.emailAddress,
                  cursorColor: AppColors.primary,
                  decoration: InputDecoration(
                    labelText: 'CORREO ELECTRÓNICO',
                    hintText: 'Correo electronico',
                  ),
                  onChanged: p.setEmail,
                  validator: emailValidator,
                ),
                SizedBox(height: size.height * .02),
                TextFormField(
                  enableInteractiveSelection: false,
                  cursorColor: AppColors.primary,
                  obscureText: true,
                  decoration: InputDecoration(
                    labelText: 'CONTRASEÑA',
                    hintText: 'Contraseña',
                  ),
                  onChanged: p.setPassword,
                  validator: passValidator,
                ),
                SizedBox(height: size.height * .06),
                MaterialButton(
                  onPressed: () {
                    if (_formKey.currentState.validate()) {
                      DBHelper dbHelper = DBHelper();
                      dbHelper.saveUser(p.nombre, p.email, p.password);
                      ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(
                          duration: Duration(milliseconds: 2000),
                          content: const Text('Usuario Registrado'),
                        ),
                      );
                      Future.delayed(Duration(milliseconds: 2500), () {
                        Navigator.pushReplacementNamed(context, loginRoute);
                      });
                    }
                  },
                  textColor: Colors.white,
                  child: Padding(
                    padding: EdgeInsets.symmetric(
                      vertical: 19,
                    ),
                    child: Text(
                      'CREAR CUENTA',
                      style: TextStyle(
                        fontSize: size.width * .040,
                        fontWeight: FontWeight.w800,
                      ),
                    ),
                  ),
                  color: AppColors.primary,
                  minWidth: size.width * .85,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(60),
                  ),
                ),
                //
                SizedBox(height: size.height * .04),
                GestureDetector(
                  onTap: () =>
                      Navigator.pushReplacementNamed(context, loginRoute),
                  child: Text(
                    'VOLVER AL LOGIN',
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
