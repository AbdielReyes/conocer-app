import 'package:app_conocer/core/constants.dart';
import 'package:app_conocer/core/models/sector.dart';
import 'package:app_conocer/theme/colors.dart';
import 'package:app_conocer/views/organo_de_gobierno.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SectorEmpresarialPage extends StatelessWidget {
  final List<Sector> sectores = [
    Sector(
      nombre: 'Consejo Coordinador Empresarial',
      logo: 'assets/img/secretarias/Consejo_Coordinador_Empresarial.svg',
      url: 'https://www.gob.mx.sep',
    ),
    Sector(
      nombre: 'Confederación Patronal de la República Mexicana',
      logo: 'assets/img/secretarias/COPARMEX.svg',
      url: 'https://www.gob.mx.stps',
    ),
    Sector(
      nombre: 'Confederación de Cámaras Industriales',
      logo: 'assets/img/secretarias/CONCAMIN.svg',
      url: 'https://www.gob.mx.sagarpa',
    ),
    Sector(
      nombre:
          'Confederación de Cámaras Nacionales de Comercio, Servicios y Turismo',
      logo: 'assets/img/secretarias/CONCANACO.svg',
      url: 'https://www.gob.mx.sectur',
    ),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: appBar(context, svg: 'assets/img/handshake-white.svg'),
      body: orgGobBody(sectores),
    );
  }
}

ListView orgGobBody(List<Sector> sectores) {
  return ListView.builder(
    itemCount: sectores.length,
    itemBuilder: (BuildContext context, int index) {
      final sector = sectores[index];
      return Container(
        child: ListTile(
          contentPadding: EdgeInsets.all(16),
          leading: SvgPicture.asset(
            sector.logo,
            width: 65,
          ),
          title: Text('${sector.nombre}'),
          onTap: () => Navigator.popAndPushNamed(
            context,
            webPageRoute,
            arguments: sector.url,
          ),
        ),
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(
              color: AppColors.primary,
            ),
          ),
        ),
      );
    },
  );
}
