import 'package:app_conocer/core/constants.dart';
import 'package:app_conocer/theme/colors.dart';
import 'package:flutter/material.dart';

import 'organo_de_gobierno.dart';

class DirectorGeneralPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final subtitulo = TextStyle(fontWeight: FontWeight.w600);
    final size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: appBar(
        context,
        svg: 'assets/img/handshake-white.svg',
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(15.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 20.0),
            Center(
              child: CircleAvatar(
                backgroundImage: AssetImage(
                    'assets/img/Rodrigo_Alejandro_Rojas_Navarrete.jpg'),
                radius: size.width * .18,
              ),
            ),
            SizedBox(height: 20.0),
            Center(
              child: Text(
                'Rodrigo A. Rojas Navarrete',
                style: TextStyle(
                  color: AppColors.primary,
                  fontSize: size.width * .045,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
            SizedBox(height: 8.0),
            Center(
              child: Text(
                'Director General del CONOCER',
                style:
                    TextStyle(color: Colors.grey, fontSize: size.width * .035),
              ),
            ),
            SizedBox(height: 8.0),
            Divider(),
            SizedBox(height: 8.0),
            Text(
              'Formacion Academica',
              style: subtitulo,
              textAlign: TextAlign.left,
            ),
            SizedBox(height: 10.0),
            Text(
              'Licenciado en Ciencias Políticas y Administración Pública por la Universidad Iberoamericana de la Ciudad de México, es especialista en implementación de Políticas Públicas y Gobierno, cuenta con estudios de maestría en Administración Pública por el INAP y con diversos diplomados en Educación y Emprendedurismo.',
              textAlign: TextAlign.justify,
            ),
            SizedBox(height: 10.0),
            Text(
              'Trayectoria Profesional',
              style: subtitulo,
              textAlign: TextAlign.left,
            ),
            SizedBox(height: 10.0),
            Text(
              '''
Asesor Honorario de la Academia Mexicana de Economía Política.\n\nConsejero del Fondo de las Naciones Unidas para la Infancia. Ha participado en diversas publicaciones entre las que destaca su coautoría en la obra titulada: “Economía Política del México Contemporáneo”, editado por Porrúa Editorial. Integrante del Club Líderes Mexicanos.\n
A temprana edad inició su activismo político y social, ha colaborado estrechamente en la vinculación entre el sector empresarial, la iniciativa privada, las organizaciones políticas y la sociedad civil. Fue coordinador juvenil del Frente Amplio Progresista y candidato a Diputado Federal.\n
En la administración pública se ha desempeñado como asesor del Jefe de Gobierno y del Secretario de Turismo de la Ciudad de México. Dentro de la administración local fungió también como Contralor Interno de la Procuraduría Ambiental e integró el Consejo Anticorrupción de la Contraloría General del gobierno capitalino. En el sector legislativo ha colaborado como consultor parlamentario y secretario técnico de la Junta Instaladora de la Asamblea Constituyente de la Ciudad de México. Rojas fue un importante impulsor de la Reforma Política del entonces Distrito Federal, la cual culminó en la elaboración, discusión y aprobación de la primera Constitución Política de la Ciudad Capital.''',
              textAlign: TextAlign.justify,
            ),
            SizedBox(height: 10.0),
            GestureDetector(
              onTap: () => Navigator.pushNamed(context, webPageRoute,
                  arguments:
                      'https://conocer.gob.mx/directorio/direccion-general/'),
              child: Text(
                'Ver mas...',
                style: TextStyle(color: AppColors.primary),
              ),
            ),
            SizedBox(height: 8),
            Divider()
          ],
        ),
      ),
    );
  }
}
