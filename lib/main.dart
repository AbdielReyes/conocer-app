import 'package:app_conocer/core/constants.dart';
import 'package:app_conocer/core/providers/registro_usuario_provider.dart';
import 'package:app_conocer/theme/colors.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'core/providers/login_provider.dart';
import 'core/providers/usuario_provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => LoginProvider()),
        ChangeNotifierProvider(create: (_) => RegistroUsuarioProvider()),
        ChangeNotifierProvider(create: (_) => UsuarioProvider()),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Material App',
        initialRoute: splashScreenRoute,
        routes: routes,
        theme: themeApp,
      ),
    );
  }
}
