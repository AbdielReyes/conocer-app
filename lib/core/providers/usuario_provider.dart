import 'package:app_conocer/core/models/usuario.dart';
import 'package:flutter/material.dart';

class UsuarioProvider with ChangeNotifier {
  User _usuario;

  User get usuario => this._usuario;

  set usuario(User usuario) {
    this._usuario = usuario;
  }
}
