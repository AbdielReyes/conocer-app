class QueHacemos {
  final String titulo;
  final List<String> contenido;

  QueHacemos(this.titulo, this.contenido);
}

final queHacemos = [
  QueHacemos(
    '¿Qué es un estándar de competencia?',
    [
      'Es el documento oficial que sirve como referente para evaluar y certificar la competencia de las personas. El estándar de Competencia describe el conjunto de conocimientos, habilidades, destrezas y actitudes, con las que debe contar una persona para ejecutar una actividad laboral, con un alto nivel de desempeño.'
    ],
  ),
  QueHacemos(
    '¿Cómo certifico mis competencias?',
    [
      '''Primero: selecciona el Estándar de Competencia de tu interés en el Registro Nacional de Estándares de Competencias (RENEC).\n
Segundo: Identifica al Certificador Acreditado.\n
Tercero: Acude al Centro de Evaluación de tu preferencia para iniciar el proceso de evaluación con fines de certificación y demuestra con evidencia que cuentas con los conocimientos, habilidades y destreza necesarias para cumplir con lo definido en un Estándar de Competencia, sin importar como los hayas adquirido.
''',
      'https://conocer.gob.mx/certifico-mis-competencias/',
    ],
  ),
  QueHacemos('¿Cómo se lleva a cabo el proceso de evaluación?', [
    '''El candidato demuestra mediante sus desempeños, productos, conocimientos y actividades que sabe hacer dicha función conforme a lo establecido a un Estándar de Competencia.\n
La evaluación se realiza en una situación real de trabajo o bien dependiendo de las características de la función puede ser una situación laboral simulada.\n
El evaluador integrara un portafolio de evidencia y lo enviara al área certificadora para que, luego de su revisión, dictamine si la persona es competente o todavía no en esa función.\n
Si resulta competente se enviarán sus datos al CONOCER para que emita el certificado de competencia laboral.
'''
  ]),
  QueHacemos(
    '¿Necesito capacitarme para poder certificar mis competencias?',
    [
      '''No, no es requisito un curso de capacitación para evaluarse, si la persona tiene el conocimiento, las habilidades y destrezas para demostrar que es competente en dicha función laboral puede iniciar un proceso de evaluación con fines de certificación.'''
    ],
  ),
  QueHacemos(
    '¿Cuánto cuesta evaluarme y certificarme?',
    [
      'Los procesos de evaluación y certificación tienen un costo y varían dependiendo del Estándar de Competencia y del Centro de Evaluación al que se acuda.'
    ],
  ),
  QueHacemos(
    '¿Tiene vigencia mi certificación?',
    [
      'Si. El tiempo de vigencia del Certificado de Competencia en un Estándar de Competencia es distinto para cada Estándar y esta especificado en el mismo.'
    ],
  ),
  QueHacemos(
    '¿Qué institución emite el certificado?',
    [
      'El certificado de competencia es un documento emitido por el CONOCER y avalado por la Secretaria de Educación Publica a nivel nacional, en el cual se asegura que el desempeño de una persona se ajusta a lo que requieren las empresas o instituciones.'
    ],
  ),
  QueHacemos(
    'Uso de Datos Abiertos',
    [
      '''- Entidades de Certificación, Evaluación y Organismos Certificadores de Competencias laborales:\n
Se muestran las Entidades de Certificación, Evaluación y Organismos Certificadores de Competencias laborales, con su cedula de acreditación, fecha de acreditación, razón social y siglas con la que se maneja internamente.
''',
      'http://conocer.gob.mx/contenido/seccionesExtras/datos_abiertos/archivos/ece_oc.csv',
      '''- Registro Nacional de Estándares de Competencias:\n
El Registro Nacional de Estándares de Competencias es un catalogo donde puedes encontrar todos los estándares de competencia que describen, en términos de resultados, el conjunto de conocimientos, habilidades, destrezas y actitudes que requieres para realizar una actividad en el ámbito laboral, social, gobierno o educativo y es el referente que permite evaluar tus competencias y en su caso, obtener un certificado que lo respalde.
''',
      'http://conocer.gob.mx/contenido/seccionesExtras/datos_abiertos/archivos/renec.csv',
      '''- Registro Nacional de Cursos de Capacitación Basados en Estándares de Competencia:\n
El Registro Nacional de Cursos de Capacitación Basados en Estándares de Competencia es el catálogo que facilita la consulta y el acceso a los trabajadores, empresarios, sector social, sector educativo y sector gobierno a los programas de los cursos de capacitación o capacitadores independientes con bases en estándares de competencia inscritos en el Registro Nacional de Estándares de Competencia.
''',
      'http://conocer.gob.mx/contenido/seccionesExtras/datos_abiertos/archivos/renac.csv',
      '''- Comités de Gestión por Competencias:\n
Muestra los datos de los Comités de Gestión por Competencias integrados en la operación del CONOCER.
''',
      'http://conocer.gob.mx/contenido/seccionesExtras/datos_abiertos/archivos/cgc.csv'
    ],
  ),
  QueHacemos(
    'Consulta los estándares de competencia en el RENEC',
    [
      '''El Registro Nacional de Estándares de Competencias (RENEC) es el catalogo donde puedes encontrar todos los estándares disponibles, se presenta como listado o por sector productivo. Recuerda, es el referente que permite evaluar tus competencias.\n
Los Estándares de competencias son bienes públicos que pueden utilizarse por cualquier organización como referente para elevar la competitividad de los sectores productivos, social, educativo y de gobierno. Además, el RENEC genera señales de mercado claras para el sector educativo para que desarrolle y valide estructuras de aprendizaje y curriculares con base en Estándares de Competencia.\n
La consulta es pública y gratuita
''',
      'https://conocer.gob.mx/registro-nacional-estandares-competencia',
    ],
  ),
  QueHacemos(
    'Perdí mi certificado ¿cómo puedo obtener un duplicado?',
    [
      'Ingresa al Registro Nacional de Personas con Competencias Certificadas (RENAP)\n',
      'https://conocengob.mx/registro-nacional-personas-competencias-certificadas/',
      '''Teclea tu CURP y aparecerán las certificaciones con las que cuentas, así como el nombre del Organismo Certificador/Entidad de Certificación y Evaluación que participó en tu proceso. Ponte en contacto con ellos y solicita un duplicado. Estas instancias son las indicadas para realizar el trámite ante el CONOCER.''',
    ],
  ),
  QueHacemos(
    '¿Qué es el Registro Nacional de Personas con Competencias Certificadas (RENAP)?',
    [
      '''En el RENAP se encuentran todas las personas que han obtenido un certificado de competencia emitido por el CONOCER, el cual es respaldado por empresarios y trabajadores de los distintos sectores del país.\n
Para consultar tus certificaciones teclea tu CURP en el RENAP.
''',
      'https://conocer.gob.mx/registro-nacional-personas-competencias-certificadas/',
    ],
  ),
  QueHacemos(
    '¿Qué es el Registro Nacional de Cursos de Capacitación basados en Estándares de Competencia (RENAC)?',
    [
      '''Si deseas tomar un curso de capacitación, el RENAC es el catálogo donde puedes consultar la oferta de capacitación alineada a los Estándares de Competencia.\n 
Es importante precisar que los cursos de capacitación no son condicionante, ni garantía para acceder a la evaluación y/o certificación de los Estándares de Competencia inscritos en el RENEC. 
''',
      'https://conocer.gob.mx/registro-nacional-cursos-capacitacion-basados-en-estandares-competencia',
      'Los contenidos y términos de los programas de capacitación son responsabilidad exclusiva de los Centros de Capacitación o Capacitadores Independientes responsables de éstos.',
    ],
  ),
  QueHacemos(
    '¿Cuáles son los requisitos para inscribir un curso de capacitación en el RENAC?',
    [
      '''Los Centros de Capacitación o Capacitadores Independientes podrán solicitar al CONOCER la inscripción de sus programas de capacitación con base en Estándares de Competencia en el RENAC.\n 
Los requisitos son:\n 
• Que el Curso de Capacitación este basado en un Estándar de Competencia vigente en el RENEC''',
      '(https://conocer.gob.mx/registro-nacional-estandares-competencia/)',
      '• Llenar el Formulario para la inscripción de cursos de capacitación en el RENAC',
      '(https://conocer.gob.mx/contenido/transparencia/Articulo/renac.html)',
      '''• Entregar ambos documentos en las oficinas del CONOCER, en Av. Barranca del Muerto No. 275 Col. San José Insurgentes, Delegación Benito Juárez, México D.F. CP: 03900. Atención a la Subdirección de Registros Nacionales.\n
El curso será revisado y avalado por el Comité de Gestión por Competencias que elaboró el Estándar de Competencia de tu interés. Te ofrecemos una Lista de Verificación para alinea los cursos de capacitación a los Estándares de Competencia.\n''',
      'https://conocer.gob.mx/contenido/seccionesExtras/transparencia/pdfs/Lista-de-verificacion-1.pdf',
      'Te recordamos que CONOCER no certifica cursos de capacitación, ofrece un espacio de difusión para los capacitadores que diseñan cursos de capacitación basados en los Estándares de Competencias inscritos en el RENEC.\n',
      'http://148.244.170.140/templates/conocer/renac.html',
    ],
  ),
  QueHacemos(
    '¿Qué hacemos en el CONOCER?',
    [
      'El CONOCER coordina y promueve el Sistema Nacional de Competencias para que México cuente con empresarios, trabajadores, docentes, estudiantes y servidores públicos más competentes.\n',
    ],
  ),
  QueHacemos(
    '¿Qué es el Sistema Nacional de Competencias (SNC)?',
    [
      'El Sistema Nacional de Competencias facilita los mecanismos para que organizaciones e instituciones públicas y privadas, cuenten con personas más competentes.\n',
      'https://conocer.gob.mx/acciones_programas/sistema-nacional-competencias',
    ],
  ),
  QueHacemos(
    '¿Cuáles son los beneficios del sistema nacional de competencia (SNC)?',
    [
      '''• Fortalece al sector empresarial al impulsar la contratación de personal competente en funciones clave. 
• Facilita la movilidad laboral de los trabajadores en sus sectores, al reconocer la autoridad educativa del país, sus competencias por medio de un certificado de SEP-CONOCER.
• Contribuye a mejorar la alineación de la oferta educativa con los requerimientos de los sectores productivos y con ello se amplían las posibilidades de los jóvenes de integrarse al mercado laboral. 
• Impulsa convenios internacionales orientados a la homologación de certificaciones para asegurar la movilidad laboral dentro y fuera del país, con base en competencias de personas. 
''',
      'https://conocer.gob.mx/acciones_programas/sistema-nacional-competencias',
    ],
  ),
  QueHacemos(
    '¿Cómo funciona el SNC?',
    [
      'El Sistema Nacional de Competencias facilita los mecanismos para que organizaciones e instituciones públicas y privadas, cuenten con personas más competentes.\n',
      'https://conocer.gob.mx/acciones_programas/sistema-nacional-competencias',
    ],
  ),
  QueHacemos(
    '¿Cómo opera la Red de Prestadores de Servicios? ',
    [
      '''Existen cuatro figuras\n 
• Entidades de Certificación y Evaluación (ECE) 
• Organismos Certificadores (OC) 
• Centros de Evaluación (CE) 
• Evaluadores Independientes (El)\n 
¿En qué se distinguen?\n 
En el alcance de sus funciones\n 
• Las ECE son organizaciones o instituciones públicas o privadas autorizadas por el CONOCER para capacitar, evaluar y certificar las competencias de las personas con base en los Estándares de Competencia inscritos en la Registro Nacional de Estándares de Competencia (RENEC). 
• Los OC son organizaciones o instituciones públicas o privadas autorizadas por el CONOCER para certificar las competencias de las personas. 
• Los CE son personas morales autorizadas por el CONOCER y acreditadas por una ECE u OC, para evaluar las competencias de las personas.
• Los El son personas físicas autorizadas por el CONOCER y acreditadas por una ECE u OC, para evaluar las competencias de las personas
''',
    ],
  ),
  QueHacemos(
    '¿Cuáles son los requisitos para constituirse como una ECE/OC?',
    [
      '',
      'https://conocer.gob.mx/contenido/transparencia/Articulo/requisitos.html',
      '''Para conocer a detalle el proceso de postulación como ECE u OC, es necesario establecer contacto con el personal de CONOCER a través de los siguientes teléfonos:\n 
Subdirección de Promoción para Prestadores de Servicios. (01 55) 22 82 20 45\n 
Jefatura de Promoción de Entidades de Certificación. (01 55) 22 82 02 00 Ext. 1065
''',
    ],
  ),
  QueHacemos(
    '¿Cuáles son los requisitos para acreditarse como Centro de Evaluación (CE) y Evaluador Independiente (El)?',
    [
      '',
      'https://conocer.gob.mx/wp-content/uploads/2017/04/Acreditacion-para-CE-y-E1.pdf',
    ],
  ),
  QueHacemos(
    'Consulta la normatividad ',
    [
      '',
      'https://conocer.gob.mx/wp-content/uploads/2017/04/reglas_generales_criterios.pdf',
    ],
  ),
];
