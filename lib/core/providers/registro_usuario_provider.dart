import 'package:flutter/material.dart';

class RegistroUsuarioProvider with ChangeNotifier {
  String _nombre;
  String _email;
  String _password;

  get email => this._email;

  void setEmail(String value) {
    this._email = value.toLowerCase();
    notifyListeners();
  }

  get password => this._password;

  void setPassword(String value) {
    this._password = value.toLowerCase();
    notifyListeners();
  }

  get nombre => this._nombre;

  void setNombre(String value) {
    this._nombre = value;
    notifyListeners();
  }
}
