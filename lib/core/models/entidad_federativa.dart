import 'dart:convert';

List<EntidadFederativa> entidadFederativaFromJson(String str) =>
    List<EntidadFederativa>.from(
        json.decode(str).map((x) => EntidadFederativa.fromJson(x)));

String entidadFederativaToJson(List<EntidadFederativa> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class EntidadFederativa {
  EntidadFederativa(
    this.id,
    this.name,
  );

  int id;
  String name;

  factory EntidadFederativa.fromJson(Map<String, dynamic> json) =>
      EntidadFederativa(
        json["id"],
        json["name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
      };
}
