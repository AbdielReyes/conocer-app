class Sector {
  final String nombre;
  final String logo;
  final String url;

  Sector({
    this.nombre,
    this.logo,
    this.url,
  });
}
