class User {
  final String id;
  final String name;
  final String email;
  final String password;
  final String phone;
  final String institucion;
  final String facebook;
  final String twitter;
  final String linkedin;

  User({
    this.phone = 'Teléfono',
    this.institucion = 'Institución',
    this.facebook = 'http://facebook.com/miperfil',
    this.twitter = 'http://twitter.com/miperfil',
    this.linkedin = 'http://linkedin.com/miperfil',
    this.id,
    this.name,
    this.email,
    this.password,
  });

  User copyWith({
    String id,
    String name,
    String email,
    String password,
    String phone,
    String institucion,
    String facebook,
    String twitter,
    String linkedin,
  }) =>
      User(
        id: id ?? this.id,
        name: name ?? this.name,
        email: email ?? this.email,
        password: password ?? this.password,
        phone: phone ?? this.phone,
        institucion: institucion ?? this.institucion,
        facebook: facebook ?? this.facebook,
        twitter: twitter ?? this.twitter,
        linkedin: linkedin ?? this.linkedin,
      );

  factory User.fromJson(Map<String, dynamic> json) => User(
        id: json['id'],
        name: json['name'],
        email: json['email'],
        password: json['password'],
        phone: json['phone'],
        institucion: json['institucion'],
        facebook: json['facebook'],
        twitter: json['twitter'],
        linkedin: json['linkedin'],
      );

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'email': email,
        'password': password,
        'phone': phone,
        'institucion': institucion,
        'facebook': facebook,
        'twitter': twitter,
        'linkedin': linkedin,
      };
}
