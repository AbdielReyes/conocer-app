import 'package:app_conocer/core/models/usuario.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'dart:io' as io;

class DBHelper {
  static final DBHelper _instance = DBHelper.internal();
  DBHelper.internal();
  factory DBHelper() => _instance;
  static Database _db;

  Future<Database> get db async {
    if (_db != null) return _db;
    _db = await initDB();
    return _db;
  }

  Future<Database> initDB() async {
    io.Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, 'fworkout.db');
    Database db = await openDatabase(
      path,
      version: 1,
      onCreate: _createTables,
    );
    print('Initdb: Success');
    return db;
  }

  void _createTables(Database db, int version) async {
    await db.execute('CREATE TABLE User('
        'id INTEGER PRIMARY KEY AUTOINCREMENT, '
        'name TEXT, '
        'email TEXT, '
        'password TEXT, '
        'phone TEXT, '
        'institucion TEXT, '
        'facebook TEXT, '
        'twitter TEXT, '
        'linkedin TEXT'
        ')');
    print('_createTables: Success');
  }

  void saveUser(String name, String email, String password) async {
    var dbClient = await db;
    await dbClient.transaction((trans) async {
      return await trans.rawInsert(
        'INSERT INTO User(name, email, password) VALUES(\'$name\', \'$email\', \'$password\')',
      );
    });
    print('aveUser: Success| $name, $email, $password');
  }

  Future<List<User>> getUser(String email, String password) async {
    var dbClient = await db;
    List<User> userList = [];
    List<Map> queryList = await dbClient.rawQuery(
      'SELECT * FROM User WHERE email=\'$email\' AND password=\'$password\'',
    );
    print('getUser: ${queryList.length} users');
    if (queryList != null && queryList.length > 0) {
      for (int i = 0; i < queryList.length; i++) {
        userList.add(User(
          id: queryList[i]['id'].toString(),
          name: queryList[i]['name'],
          email: queryList[i]['email'],
          password: queryList[i]['password'],
        ));
      }
      print('[DBHelper] getUser: ${userList[0].name}');
      return userList;
    } else {
      print('[DBHelper] getUser: User is null');
      return null;
    }
  }

  Future<int> updateUser(User user) async {
    var dbClient = await db;
    var res = await dbClient
        .update('User', user.toJson(), where: 'id? = ?', whereArgs: [user.id]);
    return res;
  }
}
