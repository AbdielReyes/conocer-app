import 'package:app_conocer/views/contactos.dart';
import 'package:app_conocer/views/director_general.dart';
import 'package:app_conocer/views/estadisticas.dart';
import 'package:app_conocer/views/estadisticas_mes.dart';
import 'package:app_conocer/views/home.dart';
import 'package:app_conocer/views/login.dart';
import 'package:app_conocer/views/organo_de_gobierno.dart';
import 'package:app_conocer/views/que_hacemos.dart';
import 'package:app_conocer/views/red_conocer.dart';
import 'package:app_conocer/views/redes_sociales.dart';
import 'package:app_conocer/views/registro_usuario.dart';
import 'package:app_conocer/views/sector_empresarial.dart';
import 'package:app_conocer/views/sector_gobierno.dart';
import 'package:app_conocer/views/estandares.dart';
import 'package:app_conocer/views/sector_laboral.dart';
import 'package:app_conocer/views/encuestas.dart';
import 'package:app_conocer/views/eventos.dart';
import 'package:app_conocer/views/splash_screen.dart';
import 'package:app_conocer/views/terminos_y_condiciones.dart';
import 'package:app_conocer/views/perfil.dart';
import 'package:app_conocer/views/webview.dart';
import 'package:app_conocer/views/informe_trimestral.dart';
import 'package:app_conocer/views/prestadores_de_servicios.dart';
import 'package:flutter/material.dart';

final String version = '2.1.0';

// Routes
final String splashScreenRoute = '/splash';
final String loginRoute = '/login';
final String registroRoute = '/registro';
final String homeRoute = '/home';
final String organoDeGobiernoRoute = '/organoDeGobierno';
final String directorGeneralRoute = '/directorGeneralConocer';
final String sectorGobiernoRoute = '/sectorGobierno';
final String sectorEmpresarialRoute = '/sectorEmpresarial';
final String sectorLaboralRoute = '/sectorLaboral';
final String webPageRoute = '/webPage';
final String queHacemosRoute = '/queHacemos';
// final String queHacemosItem = '/queHacemosItem';
final String contactoRoute = '/contacto';
final String terminosCondicionesRoute = '/terminos';
final String estadisticasRoute = '/estadisticas';
final String estadisticasMesRoute = '/estadisticasMes';
final String estandaresRoute = '/estandares';
final String encuestasRoute = '/encuestas';
final String eventosRoute = '/eventos';
final String redConocerRoute = '/redConocer';
final String redesSocialesRoute = '/redesSociales';
final String informeTrimestralRoute = '/informeTrimestral';
final String prestadoresServiciosRoute = '/prestadoresServicios';
final String perfilRoute = '/perfil';

final routes = <String, WidgetBuilder>{
  splashScreenRoute: (BuildContext context) => new SplashScreen(),
  loginRoute: (BuildContext context) => new LoginPage(),
  registroRoute: (BuildContext context) => new RegistroUsuarioPage(),
  homeRoute: (BuildContext context) => new HomePage(),
  organoDeGobiernoRoute: (BuildContext context) => new OrganoDeGobiernoPage(),
  directorGeneralRoute: (BuildContext context) => new DirectorGeneralPage(),
  sectorGobiernoRoute: (BuildContext context) => new SectorGobiernoPage(),
  sectorEmpresarialRoute: (BuildContext context) => new SectorEmpresarialPage(),
  sectorLaboralRoute: (BuildContext context) => new SectorLaboralPage(),
  webPageRoute: (BuildContext context) => new WebPage(),
  queHacemosRoute: (BuildContext context) => new QueHacemosPage(),
  // queHacemosItem: (BuildContext context) => new QueHacemosItem(),
  contactoRoute: (BuildContext context) => new ContactoPage(),
  estadisticasRoute: (BuildContext context) => new EstadisticasPage(),
  estadisticasMesRoute: (BuildContext context) => new EstadisticasMesPage(),
  estandaresRoute: (BuildContext context) => new EstandaresPage(),
  encuestasRoute: (BuildContext context) => new EncuestasPage(),
  eventosRoute: (BuildContext context) => new EventosPage(),
  redConocerRoute: (BuildContext context) => new RedConocerPage(),
  perfilRoute: (BuildContext context) => new PerfilPage(),
  redesSocialesRoute: (BuildContext context) => new RedesSocialesPage(),
  informeTrimestralRoute: (BuildContext context) => new InformeTrimestralPage(),
  prestadoresServiciosRoute: (BuildContext context) =>
      new PrestadoresServiciosPage(),
  terminosCondicionesRoute: (BuildContext context) =>
      new TerminosCondicionesPage(),
};
