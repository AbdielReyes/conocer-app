String emailValidator(String email) {
  Pattern pattern =
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
  RegExp regExp = new RegExp(pattern);
  if (regExp.hasMatch(email)) {
    return null;
  }
  return 'Correo Invalido';
}

String passValidator(String number) {
  if (number == null || number.isEmpty || number.length <= 6) {
    return 'Al menos 6 caracteres';
  }
  return null;
}

String nameValidator(String name) {
  if (name == null || name.isEmpty || name.length <= 2) {
    return 'Nombre Invalido';
  }
  return null;
}
