import 'package:flutter/material.dart';

class AppColors {
  static const Color primary = Color(0xFFCD2327);
}

final themeApp = ThemeData(
  primaryColor: AppColors.primary,
  accentColor: AppColors.primary,
);
